package main;
import dataAccess.connection.ConnectionFactory;
import presentation.controller.Controller;
import model.*;

/**
 * Main CLass
 */
public class Main {
    /**
     * metoda Main
     * @param args
     */
    public static void main(String args[]){
        ConnectionFactory.getConnection();
        Client client=new Client(3,"andrei","0742666069");
        new Controller();
    }
}
