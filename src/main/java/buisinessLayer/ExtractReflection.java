package buisinessLayer;
import java.lang.reflect.Field;

public class ExtractReflection {
    // extragere prin Reflection
    public static String retrieveProprieties(Object object)
    {
        StringBuilder sb = new StringBuilder(); // sir de returnat
        for(Field field : object.getClass().getDeclaredFields()) // toate campurile
        {
            field.setAccessible(true); // il fac accesibil
            Object value;
            try{
                value=field.get(object); // iau valoare din obiect
                if(value instanceof String) // verific instanta
                {
                    sb.append("'"+value+"'"+","); // construiesc
                }else
                    sb.append(value+",");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        sb.deleteCharAt(sb.length()-1); // merg pe ultimul char -1
        System.out.println(sb.toString());
        return sb.toString(); // returnez
    }
}
