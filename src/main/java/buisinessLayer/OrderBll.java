package buisinessLayer;

import com.itextpdf.text.pdf.PdfWriter;
import dataAccess.ClientDao;
import dataAccess.ProductDao;
import model.Order;
import com.itextpdf.text.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static java.lang.Math.abs;


/**
 * Clasa de operatii pentru Comenzi si pentru selectare elemente din coada
 */
public class OrderBll {
    private String[] productName;
    private String[] clientName;
    private static final Font font4 = FontFactory.getFont(FontFactory.TIMES_BOLD, 16, BaseColor.BLACK);
    private static final Font font1 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.BLACK);
    private static final Font font2 = FontFactory.getFont(FontFactory.TIMES_BOLD, 34, BaseColor.BLACK);
    private static final Font font3 = FontFactory.getFont(FontFactory.TIMES_ROMAN, 16, BaseColor.RED);

    /**
     * Constructor pentru extragere de date
     */
    public OrderBll()
    {
        ClientDao clientDao=new ClientDao();
        ProductDao productDao=new ProductDao();
        clientName = clientDao.getAllElements("*");
        productName = productDao.getAllElements("*");
    }

    /**
     * getter productName
     * @return productName
     */
    public String[] getProductName() {
        return productName;
    }

    /**
     * getter clientName
     * @return clientName
     */
    public String[] getClientName() {
        return clientName;
    }

    /**
     * metoda de creare bilet de comanda
     * @param order
     * @throws FileNotFoundException
     * @throws DocumentException
     */
    public static void createBill(Order order) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        String title = "Order_Invoice_"+abs(order.getIdo())+"_"+order.getClientName()+".pdf"; //  crearea de pdf
        PdfWriter.getInstance(document, new FileOutputStream(title)); // titlu
        document.open();
        // metoda de creare a chitantei
        document.addTitle("Order Invoice: "+order.getIdo()); // adaugarea campurilor in pdf
        document.add(OrderBll.getChunck("              Order Invoice: "+abs(order.getIdo())+"\n", font2));
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));
        document.add(OrderBll.getChunck("Client name: "+order.getClientName(), font1));
        document.add(new Paragraph("\n"));
        document.add(OrderBll.getChunck("Client ID: "+order.getIdc(), font1));
        document.add(new Paragraph("\n"));
        document.add(OrderBll.getChunck("Client Phone Nr. : "+order.getTel(), font1));
        document.add(new Paragraph("\n"));
        document.add(new Paragraph("\n"));
        document.add(OrderBll.getChunck("Product Ordered: "+order.getProductName(), font1));
        document.add(new Paragraph("\n"));
        document.add(OrderBll.getChunck("Product ID: "+order.getIdp(), font1));
        document.add(new Paragraph("\n"));
        document.add(OrderBll.getChunck("Quantity : ", font1));
        document.add(OrderBll.getChunck(String.valueOf(order.getQuantity()),font3));
        document.add(new Paragraph("\n"));
        SimpleDateFormat formatter= new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        document.add(OrderBll.getChunck("Date : ", font1));
        document.add(OrderBll.getChunck(formatter.format(date),font4));
        document.close(); // inchidere
    }

    /**
     * creare Chunk
     * @param name
     * @param font
     * @return
     */
    public static Chunk getChunck(String name,Font font)
    {
        return new Chunk(name,font);
    }
}  // metoda de creare de chunck
