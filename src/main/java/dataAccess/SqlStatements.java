package dataAccess;
import buisinessLayer.ExtractReflection;

/**
 * Clasa SQL Statements
 */
public class SqlStatements {
    /**
     * Metoda de create editare
     * @param name
     * @param value
     * @param table
     * @return
     */
    public static String createEdit(String name,String value,String table)
    {   // metoda creare interogare
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(table);
        sb.append(" SET quantity=");
        sb.append(value);
        sb.append(" WHERE name='");
        sb.append(name);
        sb.append("'");
        return sb.toString();
    }

    /**
     * Metoda de create insert
     * @param object
     * @param table
     * @return
     */
    public  static String createInsert(Object object,String table) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append("`"+table+"`")
        ;
        sb.append(" VALUES (");
        sb.append(ExtractReflection.retrieveProprieties(object));
        sb.append(")");
        return sb.toString();
    }

    /**
     * Metoda de create select
     * @param rows
     * @param table
     * @return
     */
    public static String createSelect(String rows,String table)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(rows);
        sb.append(" FROM ");
        sb.append(table);
        return sb.toString();
    }

    /**
     * Metoda de create delete
     * @param id
     * @param field
     * @param table
     * @return
     */
    public static String createDelete(String id,String field,String table)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(table);
        sb.append(" WHERE ");
        sb.append(field);
        sb.append("=");
        sb.append(id);
        return sb.toString();
    }

    /**
     * Metoda de create selectWhere
     * @param field
     * @param fieldCondition
     * @param condition
     * @param table
     * @return
     */
    public static String createSelectWhere(String field,String fieldCondition,String condition,String table)
    {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(field);
        sb.append(" FROM ");
        sb.append(table);
        sb.append(" WHERE ");
        sb.append(fieldCondition);
        sb.append("='");
        sb.append(condition);
        sb.append("'");
        return sb.toString();
    }
}
