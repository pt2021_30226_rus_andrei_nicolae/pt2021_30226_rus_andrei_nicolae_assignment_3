package dataAccess;
import buisinessLayer.ExtractReflection;
import dataAccess.connection.ConnectionFactory;
import javax.swing.*;
import java.beans.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
/**
 * clasa Generica pentru executarea operatiilor si pt Reflection
 * @param <T>
 */
public class Dao<T> {
    protected final Class<T> type; // tipul de clasa
    private ResultSet resultSet;
    /**
     * Constructor pentru tip
     */
    @SuppressWarnings("unchecked")
    public Dao() { // constructoe
        this.type=(Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0] ;
    }
    /**
     * returneaza tipul de clasa
     * @return type
     */
    public Class<T> getType() {
        return type;
    }
    /**
     * Clasa ce executa editarea
     * @param name
     * @param value
     */
    public void executeEdit(String name,String value) // executare editare
    {
        Connection connection=null; // init conexiune
        PreparedStatement statement=null; // interogare
        String query= SqlStatements.createEdit(name,value,type.getSimpleName()); // creare interogare
        try
        {
            connection= ConnectionFactory.getConnection(); // facere conexiune
            statement= connection.prepareStatement(query);
            statement.executeUpdate(); // executare
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    } // la fel jos, ca mai inainte
    /**
     * metoda de executie select where
     * @param field
     * @param fieldCondition
     * @param condition
     * @return s
     */
    public String executeCreateSelectWhere(String field,String fieldCondition,String condition)
    {
        Connection connection=null;
        PreparedStatement statement=null;
        resultSet=null;
        String query=SqlStatements.createSelectWhere(field,fieldCondition,condition,type.getSimpleName());
        try
        {
            connection=ConnectionFactory.getConnection();
            statement= connection.prepareStatement(query);
            resultSet= statement.executeQuery();
            if(resultSet.next()) {
                String s = resultSet.getString(1);
                return s;
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    /**
     * metoda de executare sql select where cu T
     * @param field
     * @param fieldCondition
     * @param condition
     * @return s
     */
    public T executeCreateSelectWhereT(String field,String fieldCondition,String condition)
    {
        Connection connection=null;
        PreparedStatement statement=null;
        resultSet=null;
        String query=SqlStatements.createSelectWhere(field,fieldCondition,condition,type.getSimpleName());
        try
        {
            connection=ConnectionFactory.getConnection();
            statement= connection.prepareStatement(query);
            resultSet= statement.executeQuery();

            return createObjects(resultSet).get(0);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    /**
     * Metoda de executare sql add
     * @param object
     * @return T
     */
    public T executeAdd(Object object)
    {
        Connection connection=null;
        PreparedStatement statement=null;
        String query=SqlStatements.createInsert(object,type.getSimpleName());
        try
        {
            connection=ConnectionFactory.getConnection();
            statement= connection.prepareStatement(query);
            System.out.println(query);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    /**
     * Metoda de executie sql delete
     * @param id
     * @param field
     * @return T
     */
    public T executeDelete(String id,String field)
    {
        Connection connection=null;
        PreparedStatement statement=null;
        String query=SqlStatements.createDelete(id,field,type.getSimpleName());
        try
        {
            connection=ConnectionFactory.getConnection();
            statement= connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    /**
     * Metoda de executie sql view
     * @param rows
     * @return JTable
     */
    public JTable executeView(String rows)
    {
        Connection connection=null;
        PreparedStatement statement=null;
        resultSet=null;
        String query=SqlStatements.createSelect(rows,type.getSimpleName());
        try
        {
            connection=ConnectionFactory.getConnection();
            statement= connection.prepareStatement(query);
            resultSet= statement.executeQuery();
            JTable table=new JTable(getRows(resultSet),getColumn());
            return table;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    /**
     * metoda de ecexutare sql viewResult
     * @param rows
     * @return List T
     */
    public List<T> executeViewResult(String rows) // metode de executare vedere
    {   //  se face aici fiecare interogare // la fel aceeasi explicatie ca sus
        Connection connection=null;
        PreparedStatement statement=null;
        resultSet=null;
        String query=SqlStatements.createSelect(rows,type.getSimpleName());
        try
        {
            connection=ConnectionFactory.getConnection();
            statement= connection.prepareStatement(query);
            resultSet= statement.executeQuery();
            List<T> list= createObjects(resultSet);
            return list;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
    /**
     * Metoda de returnare a campurilor de head prin reflection
     * @return colums
     */
    private String[] getColumn() // ia coloanele din tabele
    {
        String[] columns = new String[type.getFields().length];
        int i=0;
        for(Field field : type.getDeclaredFields()) // pe campuri
        {
            columns[i] = field.getName(); // iau fiecare nume
            i++;
        }
        return columns;
    }
    /**
     * Metoda ce returneaza randurile cu valori pentru ficare tabel
     * @param resultSet
     * @return rows
     */
    private String[][] getRows(ResultSet resultSet)  { // returneaza liniile de rezultat
        String[][] rows = new String[12][12]; // sir de linii
        int i=0,j=0;
        List<T> list= createObjects(resultSet); // lista de obiecte
        for(T object: list) // merge pe T
        {
            for(Field field : type.getDeclaredFields()) // toate campurile
            {
                try {
                    rows[i][j] = field.get(object).toString(); // creez campuri
                    j++;
                }catch ( IllegalAccessException e)
                {
                    e.printStackTrace();
                }
            }
            i++;
            j=0;
        }
        return rows;
    }
    /**
     * Metoda de creare obiecte prin reflection in functie de ResultSet
     * @param resultSet
     * @return List T
     */
    protected List<T> createObjects(ResultSet resultSet)
    {   // create de obiecte Reflection
        List<T> list = new ArrayList<T>(); // lista de returnat
        try {
            while(resultSet.next()) // rezultate vizibile
            {
                T instance = type.getDeclaredConstructor().newInstance(); // creez instanta
                for(Field field : type.getDeclaredFields())// pe toate campurile
                {
                    Object value = resultSet.getObject(field.getName()); // iau numele de acolo
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type,"get"+type.getSimpleName(),"set"+field.getName());
                    Method method = propertyDescriptor.getWriteMethod(); //  despic sub forma de setter metoda
                    method.invoke(instance,value); // o invoc
                }
                list.add(instance); // adaug
            }
        } catch (SQLException | InstantiationException | IllegalAccessException | IntrospectionException | InvocationTargetException | NoSuchMethodException  throwables) {
            throwables.printStackTrace();
        }
        return list;
    }
    /**
     * Metoda de returnare a tuturor elementelor dintr-o Tabela in sir
     * @param field
     * @return String[]
     */
    public String[] getAllElements(String field) // returnare toate campuri si extragere - reflection
    {
        String[] strings=new String[executeViewResult(field).size()];
        int i=0;
        for(T object : executeViewResult(field) ) {
            strings[i] = getName(ExtractReflection.retrieveProprieties(object));
            i++;
        }
        return strings;
    }

    /**
     * Metoda de selectare a numelui din sir
     * @param s
     * @return string
     */
    private String getName(String s) // metoda de selectare de nume campuri si separare
    {
        String string;
        string=s;
        String segments[]= string.split("'");
        return segments[1];
    }
}
