package dataAccess.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clasa ce efectueaza conexiunea cu baza de date
 */
public class ConnectionFactory {
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName()); // logger
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";  // driver
    private static final String DBURL =  "jdbc:mysql://localhost:3306/schooldb"; // dbrul
    private static final String USER = "root";  // radacina
    private static final String PASS = "Wizzair.com"; // parola

    private static ConnectionFactory singleInstance = new ConnectionFactory(); // instanta conexiune

    /**
     * Constructor Connection
     */
    public ConnectionFactory(){ // metoda constructor
        try{
            Class.forName(DRIVER);
            System.out.println("M am conectat!" );
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    /**
     * Constructor Connextion
     * @return connection
     */
    private Connection createConnection() { // creare conexiune
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(DBURL, USER, PASS);
        }catch (SQLException e){
            LOGGER.log(Level.WARNING, "An error occurred while trying to connect to the database"); // cod eraore
        }
        return connection;
    }

    /**
     * preluare conexiune
     * @return connection
     */
    public static Connection getConnection()  {
        return singleInstance.createConnection();
    }
    // getter conexiune
}
