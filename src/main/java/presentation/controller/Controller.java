package presentation.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import presentation.view.*;

/**
 * Clasa generala Controller
 */
public class Controller {
    private ViewStart viewStart;

    /**
     * Constructor Controller
     */
    public Controller()
    {
        viewStart=new ViewStart();
        viewStart.addListenerClient(new ClientListener());
        viewStart.addListenerProduct(new ProductListener());
        viewStart.addListenerOrder(new OrderListener());
    }

    /**
     * Clasa buton client ascultator
     */
    class ClientListener implements ActionListener{

        /**
         * Deschidere fereastra pentru Clienti
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            new ControllerClients();
        }
    }

    /**
     * Clasa buton product ascultator
     */
    class ProductListener implements ActionListener{

        /**
         * Deschidere fereastra pentru Product
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            new ControllerProducts();
        }
    }

    /**
     * Clasa buton comenzi ascultator
     */
    class OrderListener implements ActionListener{
        /**
         * Deschidere fereastra pentru Order
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
           new ControllerOrders();
        }
    }
}
