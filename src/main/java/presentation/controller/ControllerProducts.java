package presentation.controller;
import dataAccess.ProductDao;
import model.Product;
import presentation.view.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clasa control produse
 */
public class ControllerProducts {
    private ViewProduct viewProduct;

    /**
     * constructor
     */
    public ControllerProducts()
    {
        viewProduct=new ViewProduct();
        viewProduct.addListenerViewAll(new ViewAll());
        viewProduct.addListenerAdd(new Add());
        viewProduct.addListenerEdit(new Edit());
        viewProduct.addListenerDelete(new Delete());
    }

    /**
     * clasa ascultator ViewAll
     */
    class ViewAll implements ActionListener {
        /**
         * metoda buton ascultator ViewALl
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            ProductDao client =new ProductDao();
            viewProduct.addTable(client.executeView("*"));
            //viewClient.addTable(client.executeView());
        }
    }

    /**
     * clasa ascultator Edit
     */
    class Edit implements ActionListener{
        /**
         * metoda buton Edit
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            ProductDao productDao=new ProductDao();
            productDao.executeDelete(String.valueOf(viewProduct.getIdp()), "idp");
            Product product=new Product(viewProduct.getIdp(), viewProduct.getName(), viewProduct.getQuantity());
            productDao.executeAdd(product);
        }
    }

    /**
     * clasa ascultator Add
     */
    class Add implements ActionListener{
        /**
         * metoda buton Add
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            Product product=new Product(viewProduct.getIdp(), viewProduct.getName(), viewProduct.getQuantity());
            ProductDao productDao=new ProductDao();
            productDao.executeAdd(product);
        }
    }

    /**
     * Clasa ascultator Delete
     */
    class Delete implements ActionListener{
        /**
         * metoda buton Delete
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            ProductDao productDao=new ProductDao();
            productDao.executeDelete(String.valueOf(viewProduct.getIdp()), "idp");
        }
    }
}
