package presentation.controller;
import buisinessLayer.OrderBll;
import com.itextpdf.text.DocumentException;
import dataAccess.ClientDao;
import dataAccess.OrderDao;
import dataAccess.ProductDao;
import model.Client;
import model.Order;
import model.Product;
import presentation.view.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.Random;

/**
 * Clasa de control Comenzi
 */
public class ControllerOrders {
    private ViewOrder viewOrder;
    private ProductDao productDao;
    private ClientDao clientDao;
    private Order order;

    /**
     * Constructor
     */
    public ControllerOrders()
    {
        OrderBll orderBll = new OrderBll();
        viewOrder=new ViewOrder(orderBll.getClientName(), orderBll.getProductName());
        viewOrder.addConfirmListener(new Confirm());
        productDao=new ProductDao();
        clientDao=new ClientDao();
    }

    /**
     * Clasa ascultator buton Confirm
     */
    class Confirm implements ActionListener {
        /**
         * ascultator confirm
         * @param event
         */
        public void actionPerformed(ActionEvent event) // executare comanda
        {
            String clientName = viewOrder.getClientSelection(); // preluare client
            String productName= viewOrder.getProductSelection();// preluare produs
            int quantity = Integer.parseInt(viewOrder.getQuantity()); // cantitate
            int existingQuantity = Integer.parseInt(productDao.executeCreateSelectWhere("quantity","name",productName));
            if(quantity > existingQuantity) // daca cant select > exis
                JOptionPane.showMessageDialog(null, "Please select a smaller quantity or update stocks!",   "ERROR : Insufficient products", JOptionPane.INFORMATION_MESSAGE);
            else { // daca e ok, fac comanda
                productDao.executeEdit(productName,String.valueOf(existingQuantity-quantity));
                JOptionPane.showMessageDialog(null, "Order registered successfully!",   "Success", JOptionPane.INFORMATION_MESSAGE);
                // executare editare
                Product product= productDao.executeCreateSelectWhereT("*","name", viewOrder.getProductSelection());
                Client client= clientDao.executeCreateSelectWhereT("*","username", viewOrder.getClientSelection());
                // selectare where
                // init order
                Random random=new Random();
                order=new Order(random.nextInt()%9000+1000,client.getIdc(),product.getIdp(),clientName,client.getTel(),productName,quantity);
                OrderDao orderDao = new OrderDao();
                orderDao.executeAdd(order);
                try {
                    OrderBll.createBill(order);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
