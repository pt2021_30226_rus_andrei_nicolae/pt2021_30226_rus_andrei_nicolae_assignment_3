package presentation.controller;
import dataAccess.ClientDao;
import model.Client;
import presentation.view.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Clasa Controller Vedere Clienti
 */
public class ControllerClients {
    private ViewClient viewClient;

    /**
     * Constructor ControllerClienti
     */
    public ControllerClients()
    {
        viewClient=new ViewClient();
        viewClient.addListenerViewAll(new ViewAll());
        viewClient.addListenerAdd(new Add());
        viewClient.addListenerEdit(new Edit());
        viewClient.addListenerDelete(new Delete());
    }

    /**
     * Clasa ascultator pentru ViewAll
     */
    class ViewAll implements ActionListener{ // executare vedere
        /**
         * ascultator buton ViewAll
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            ClientDao client =new ClientDao();
            viewClient.addTable(client.executeView("*"));
            //viewClient.addTable(client.executeView());
        }
    }

    /**
     * Clasa ascultator pentru Edit
     */
    class Edit implements ActionListener{
        /**
         * ascultator buton Edit
         * @param event
         */
        public void actionPerformed(ActionEvent event) // executare editare
        {
            ClientDao clientDao=new ClientDao();
            clientDao.executeDelete(String.valueOf(viewClient.getidc()),"idc");
            Client client=new Client(viewClient.getidc(),viewClient.getUsername(), viewClient.getTel());
            clientDao.executeAdd(client);
        }
    }

    /**
     * ascultator Add
     * Clasa ascultator pentru Add
     */
    class Add implements ActionListener{
        public void actionPerformed(ActionEvent event) // executare adaugare
        {
            Client client=new Client(viewClient.getidc(),viewClient.getUsername(), viewClient.getTel());
            ClientDao clientDao=new ClientDao();
            clientDao.executeAdd(client);
        }
    }

    /**
     * Clasa ascultator pentru Delete
     */
    class Delete implements ActionListener{
        /**
         * ascultator Delete
         * @param event
         */
        public void actionPerformed(ActionEvent event)
        {
            ClientDao clientDao=new ClientDao();
            clientDao.executeDelete(String.valueOf(viewClient.getidc()),"idc");
        }
    }
}
