package presentation.view;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Clasa pentru vederea de client
 */
public class ViewClient extends JFrame{
    JLabel idc,username,tel;
    JTextField idcT,usernameT,telT;
    JButton viewAll,add,edit,delete;
    JPanel p1,p2;
    JTable table;
    JScrollPane sp;

    /**
     * Constructor vedere client
     */
    public ViewClient()
    {
        table=new JTable();
        sp = new JScrollPane(table);
        idc=new JLabel();
        username=new JLabel();
        tel=new JLabel();
        idcT=new JTextField(5);
        usernameT=new JTextField(5);
        telT=new JTextField(5);
        viewAll=new JButton("View Clients");
        add=new JButton("Add");
        edit=new JButton("Edit");
        delete=new JButton("Delete");
        p1=new JPanel();
        p2=new JPanel();
        idc.setText("Id client:");
        username.setText("Username:");
        tel.setText("Phone:");

        p1.add(idc);
        p1.add(idcT);
        p1.add(username);
        p1.add(usernameT);
        p1.add(tel);
        p1.add(telT);
        p2.add(viewAll);
        p2.add(add);
        p2.add(edit);
        p2.add(delete);
        this.setSize(400,200);
        this.setTitle("CLIENT");
        this.setContentPane(p1);
        this.add(p2);
        this.add(sp);
        this.setVisible(true);
    }

    /**
     * adaugare buton viewAll
     * @param l
     */
    public void addListenerViewAll(ActionListener l)
    {
        viewAll.addActionListener(l);
    }

    /**
     * adaugare buton add
     * @param l
     */
    public void addListenerAdd(ActionListener l)
    {
        add.addActionListener(l);
    }

    /**
     * adaugare buton edit
     * @param l
     */
    public void addListenerEdit(ActionListener l)
    {
        edit.addActionListener(l);
    }

    /**
     * adaugare buton delete
     * @param l
     */
    public void addListenerDelete(ActionListener l)
    {
        delete.addActionListener(l);
    }

    /**
     * getter idcT
     * @return idcT
     */
    public int getidc()
    {
        return Integer.parseInt(idcT.getText());
    }

    /**
     * getter unsername
     * @return usernameT
     */
    public String getUsername()
    {
        return usernameT.getText();
    }

    /**
     * Getter tel
     * @return telT
     */
    public String getTel()
    {
        return telT.getText();
    }

    /**
     * intializare JTable
     * @param table
     */
    public void addTable(JTable table)
    {
        this.remove(sp);
        this.table=table;
        this.table.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(table);
        this.sp=sp;
        this.add(sp);
        this.sp.repaint();
    }
}
