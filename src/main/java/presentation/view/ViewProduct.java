package presentation.view;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Clasa vedere product
 */
public class ViewProduct extends JFrame{
    JLabel idp,name,quantity;
    JTextField idpT,nameT,quantityT;
    JButton viewAll,add,edit,delete;
    JPanel p1,p2;
    JTable table;
    JScrollPane sp;

    /**
     * constructor ViewProduct
     */
    public ViewProduct()
    {
        table=new JTable();
        sp = new JScrollPane(table);
        idp=new JLabel();
        name=new JLabel();
        quantity=new JLabel();
        idpT=new JTextField(5);
        nameT=new JTextField(5);
        quantityT=new JTextField(5);
        viewAll=new JButton("View Products");
        add=new JButton("Add");
        edit=new JButton("Edit");
        delete=new JButton("Delete");
        p1=new JPanel();
        p2=new JPanel();
        idp.setText("Id product:");
        name.setText("Name:");
        quantity.setText("Quantity:");

        p1.add(idp);
        p1.add(idpT);
        p1.add(name);
        p1.add(nameT);
        p1.add(quantity);
        p1.add(quantityT);
        p2.add(viewAll);
        p2.add(add);
        p2.add(edit);
        p2.add(delete);
        this.setSize(400,200);
        this.setTitle("PRODUCT");
        this.setContentPane(p1);
        this.add(p2);
        this.add(sp);
        this.setVisible(true);
    }

    /**
     * adaugare buton viewAll
     * @param l
     */
    public void addListenerViewAll(ActionListener l)
    {
        viewAll.addActionListener(l);
    }

    /**
     * adaugare buton add
     * @param l
     */
    public void addListenerAdd(ActionListener l)
    {
        add.addActionListener(l);
    }

    /**
     * adaugare buton edit
     * @param l
     */
    public void addListenerEdit(ActionListener l)
    {
        edit.addActionListener(l);
    }

    /**
     * adaugare buton delete
     * @param l
     */
    public void addListenerDelete(ActionListener l)
    {
        delete.addActionListener(l);
    }

    /**
     * getter idP
     * @return idpT
     */
    public int getIdp()
    {
        return Integer.parseInt(idpT.getText());
    }

    /**
     * getter nameT
     * @return nameT
     */
    public String getName()
    {
        return nameT.getText();
    }

    /**
     * getter quantity
     * @return quantity
     */
    public int getQuantity()
    {
        return Integer.parseInt(quantityT.getText());
    }

    /**
     * adaugare valori in JTable
     * @param table
     */
    public void addTable(JTable table)
    {
        this.remove(sp);
        this.table=table;
        this.table.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(table);
        this.sp=sp;
        this.add(sp);
        this.sp.repaint();
    }
}
