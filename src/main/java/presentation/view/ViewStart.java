package presentation.view;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Clasa vedere de start
 */
public class ViewStart extends JFrame {
    JLabel label;
    JButton client,product,order;
    JPanel p1;

    /**
     * Constructor ViewStart
     */
    public ViewStart()
    {
        label = new JLabel("Alegeti una din optiuni:  \n\n");
        client=new JButton("Editare client");
        product=new JButton("Editare produse");
        order=new JButton("Comenzi");
        p1 = new JPanel();
        p1.add(label);
        p1.add(client);
        p1.add(product);
        p1.add(order);
        this.add(p1);
        this.setSize(200,200);
        this.setVisible(true);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * adaugare buton client
     * @param l
     */
    public void addListenerClient(ActionListener l)
    {
        client.addActionListener(l);
    }

    /**
     * adaugare buton product
     * @param l
     */
    public void addListenerProduct(ActionListener l)
    {
        product.addActionListener(l);
    }

    /**
     * adaugare buton order
     * @param l
     */
    public void addListenerOrder(ActionListener l)
    {
        order.addActionListener(l);
    }
}
