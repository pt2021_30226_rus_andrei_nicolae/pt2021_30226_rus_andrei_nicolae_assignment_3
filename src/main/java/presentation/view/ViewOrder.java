package presentation.view;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Clasa ViewOrder
 */
public class ViewOrder extends JFrame{
    JComboBox clientSelection,productSelection;
    JTextField quantity;
    JLabel client,product,quantityL;
    JPanel panel;
    JButton confirm;

    /**
     * Constructor ViewOrder
     * @param clientChoices
     * @param productChoices
     */
    public ViewOrder(String [] clientChoices,String [] productChoices)
    {
        clientSelection=new JComboBox(clientChoices);
        productSelection=new JComboBox(productChoices);
        client=new JLabel("Select Client: ");
        product=new JLabel("Select Product: ");
        quantityL=new JLabel("Quantity: ");
        confirm=new JButton("Confirm");

        quantity=new JTextField(5);
        panel=new JPanel();
        this.setSize(400,200);
        this.setTitle("ORDER");
        panel.add(client);
        panel.add(clientSelection);
        panel.add(product);
        panel.add(productSelection);
        panel.add(quantityL);
        panel.add(quantity);
        panel.add(confirm);
        this.add(panel);
        this.setVisible(true);
    }

    /**
     * adaugare buton ascultator Confirm
     * @param l
     */
    public void addConfirmListener(ActionListener l)
    {
        confirm.addActionListener(l);
    }

    /**
     * getter clientSelection
     * @return clientSelection
     */
    public String getClientSelection()
    {
        return clientSelection.getSelectedItem().toString();
    }

    /**
     * Getter getProductSelection
     * @return getProductSelection
     */
    public String getProductSelection()
    {
        return productSelection.getSelectedItem().toString();
    }

    /**
     * getter quantity
     * @return quantity
     */
    public String getQuantity()
    {
        return quantity.getText();
    }
}
