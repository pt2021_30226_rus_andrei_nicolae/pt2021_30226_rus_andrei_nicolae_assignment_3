package model;

/**
 * Clasa Product
 */
public class Product {
    public int idp;
    public String name;
    public int quantity;

    /**
     * Constructor Product
     * @param idp
     * @param name
     * @param quantity
     */
    public Product(int idp,String name,int quantity)
    {
        this.idp=idp;
        this.name=name;
        this.quantity=quantity;
    }

    /**
     * Constructor Product
     */
    public Product()
    {

    }

    /**
     * Setter idp
     * @param idp
     */
    public void setidp(int idp) {
        this.idp = idp;
    }

    /**
     * Setter name
     * @param name
     */
    public void setname(String name) {
        this.name = name;
    }

    /**
     * Setter quantity
     * @param quantity
     */
    public void setquantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * Getter idp
     * @return idp
     */
    public int getIdp() {
        return idp;
    }

    /**
     * Getter name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Getter quantity
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }
}
