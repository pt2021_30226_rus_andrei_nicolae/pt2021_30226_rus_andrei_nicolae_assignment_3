package model;

/**
 * Clasa pentru comenzi
 */
public class Order {
    private int ido;
    private int idc;
    private int idp;
    private String clientName;
    private String tel;
    private String productName;
    private int quantity;

    /**
     * Constructor Order
     * @param ido
     * @param idc
     * @param idp
     * @param clientName
     * @param tel
     * @param productName
     * @param quantity
     */
    public Order(int ido,int idc,int idp,String clientName,String tel,String productName,int quantity)
    {
        this.ido=ido;
        this.idc=idc;
        this.idp=idp;
        this.clientName=clientName;
        this.tel=tel;
        this.productName=productName;
        this.quantity=quantity;
    }

    /**
     * Getter ido
     * @return ido
     */
    public int getIdo() {
        return ido;
    }

    /**
     * Getter idc
     * @return idc
     */
    public int getIdc() {
        return idc;
    }

    /**
     * getter idp
     * @return idp
     */
    public int getIdp() {
        return idp;
    }

    /**
     * Getter clientName
     * @return clientName
     */
    public String getClientName() {
        return clientName;
    }

    /**
     * Getter tel
     * @return tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * Getter productName
     * @return productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Getter quantity
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }
}
