package model;

/**
 * Clasa folosita pentru a defini fiecare clint, conform bazei dde date
 */
public class Client {
    public int idc;
    public String username;
    public String tel;

    /**
     * Constructor Client
     * @param idc
     * @param username
     * @param tel
     */
    public Client(int idc,String username,String tel)
    {
        this.idc=idc;
        this.username=username;
        this.tel=tel;
    }

    /**
     *  Constructor Client fara parametrii
     */
    public Client()
    {

    }

    /**
     * Setter idc
     * @param idc
     */
    public void setidc(int idc) {
        this.idc = idc;
    }

    /**
     * Setter username
     * @param username
     */
    public void setusername(String username) {
        this.username = username;
    }


    /**
     * Setter telefon
     * @param tel
     */
    public void settel(String tel) {
        this.tel = tel;
    }

    /**
     * Returneaza username
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returneaza idc
     * @return idc
     */
    public int getIdc() {
        return idc;
    }

    /**
     * Returneaza tel
     * @return tel
     */
    public String getTel() {
        return tel;
    }
}
